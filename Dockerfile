FROM python:3.8.5-alpine@sha256:ed9754dcd7e99761e02cb819491d86d35cd4d4a295a50e0f2e28de8a3c4cc828

LABEL maintainer="Mateus \"Doodad \" Medeiros <mateus.medeiros@ecostage.com.br>"

RUN pip install csvkit==1.0.5

CMD [ "/bin/sh" ]
